'use strict';
const { sanitizeEntity } = require('strapi-utils');


module.exports = {

    searchByLocationOrCity: async ctx => {
        ctx.log.debug(ctx);
        if (ctx.request.body && ctx.request.body.text.length>=3) {
          let text = ctx.request.body.text;
          let query = {isActive:true};
          query['name'] = { "$regex": text, "$options": "i" }
        //   ctx.log.debug(ctx.request);
          let localities = await strapi.query('locality').model.find(query);
          let cities = await strapi.query('city').model.find(query);
          ctx.log.debug(JSON.stringify(query));
          ctx.log.debug(JSON.stringify(localities));
          let response = []
         localities.forEach(local=>{
              response.push({name:local.name,id:local.id,type:'locality'});
              return true;
          });
          cities.forEach(city =>{
            response.push({name:city.name,id:city.id,type:'city'});
            return true;
          });
          return response;
        }
        return {error:"send some text man"}
        
      },

    

};
