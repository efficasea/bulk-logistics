'use strict';

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Array}
   */

  storeLocation: async ctx => {
    let entities;
    if (ctx.request.body) {
      let locality = ctx.request.body.locality;
      let query = {isActive:true};
      if(!! locality){
        query["localities"]={'$in':[locality]}
      }
      if (ctx.request.body.category){
        query['categories'] = {'$in':[ctx.request.body.category]}
      }
      entities = await strapi.query('store').model.find(query);
      ctx.log.debug(JSON.stringify(entities));
    }
    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.store }));
  },
};